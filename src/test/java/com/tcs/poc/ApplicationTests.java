package com.tcs.poc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

import com.tcs.poc.SpringBootSwaggerPocApplication;
import com.tcs.poc.model.Person;;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootSwaggerPocApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class ApplicationTests {
	@Autowired
	private TestRestTemplate restTemplate;
	
	@LocalServerPort
	private int port;
	
	private String getRootUrl() {
		return "http://localhost:" + port;
	}
	
	@Test
	public void contextLoads() { }
	
	@Test
	public void testGetAllPeople() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				getRootUrl() + "/people",
				HttpMethod.GET,
				entity,
				String.class
				);
		
		Assert.assertNotNull(response.getBody());
	}
	
	@Test
	public void testGetPersonById() {
		Person person = this.restTemplate.getForObject(getRootUrl() + "/person/1", Person.class);
		System.err.println(person.getFirstName());
		Assert.assertNotNull(person);
	}
	
	@Test
	public void testCreatePerson() {
		Person person = new Person();
		person.setFirstName("Daniela");
		person.setLastName("Mayorquín");
		person.setAge(24);
		person.setBloodGroup("O+");
		
		ResponseEntity<Person> postResponse = 
				this.restTemplate.postForEntity(getRootUrl() + "/person", person, Person.class);
		
		Assert.assertNotNull(postResponse);
		Assert.assertNotNull(postResponse.getBody());
	}
	
	@Test
	public void testUpdatePerson() {
		int id = 1;
		Person person = this.restTemplate.getForObject(getRootUrl() + "/person/" + id, Person.class);
		person.setFirstName("Alejandro");
		
		this.restTemplate.put(getRootUrl() + "/person/" + id, person);
		
		Assert.assertNotNull(
				this.restTemplate.getForObject(getRootUrl() + "/person/" + id, Person.class)
		);
	}
	
	@Test
	public void testDeletePerson() {
		int id = 2;
		Person person = this.restTemplate.getForObject(getRootUrl() + "/person/" + id, Person.class);
		Assert.assertNotNull(person);

		restTemplate.delete(getRootUrl() + "/person/" + id);

		try {
			person = restTemplate.getForObject(getRootUrl() + "/person/" + id, Person.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}
}