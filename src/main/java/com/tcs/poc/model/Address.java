package com.tcs.poc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Table(
		name = "addresses",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {"address", "district", "city", "postal_code"
						})}
)
@EntityListeners(AuditingEntityListener.class)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Address implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 12345L;

	@Id
	@Hidden
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Schema(description = "Address 1", 
            example = "Carrillo Puerto #300", required = true)
	@NotBlank
	@Size(min = 1, max = 120)
	@Column(name="address", nullable=false, length = 120)
	private String address;
	
	@Schema(description = "District of the address", 
            example = "Morelos", required = true)
	@NotBlank
	@Size(min = 1, max = 120)
	@Column(name="district", nullable=false, length = 120)
	private String district;
	
	@Schema(description = "City", 
            example = "Tepic", required = true)
	@NotBlank
	@Size(min = 1, max = 100)
	@Column(name="city", nullable=false, length = 100)
	private String city;
	
	@Schema(description = "Postal code.", 
            example = "63160", required = true)
	@NotBlank
	@Size(min = 1, max = 16)
	@Column(name="postal_code", nullable=false, length = 16)
	private String postalCode;
	
	@ManyToOne(targetEntity = Person.class, fetch = FetchType.LAZY)
	@JoinColumn(name="person_id", insertable = false, updatable = false)
	@NotNull(message = "Person not set")
	@JsonBackReference
	private Person person;
	
	@Schema(description = "Person id.", 
            example = "1", required = true)
	@NotBlank
	@Column(name = "person_id", nullable = false)
	private int personId;

	/* Getters and Setters */
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}
	
	@Override
	public String toString() {
		return "Address{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", district='" + district + '\'' +
                ", city='" + city + '\'' +
                ", postalCode=" + postalCode +
                ", person=" + person +
                '}';
	}
	
}
