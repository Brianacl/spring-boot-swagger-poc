package com.tcs.poc.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.tcs.poc.exception.ResourceNotFoundException;
import com.tcs.poc.model.Address;
import com.tcs.poc.repository.AddressRepository;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Address", description = "Address API")
public class AddressController {
	@Autowired
	private AddressRepository addressRepository;
	
	@Operation(summary = "Find all addresses")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successful operation", content = 
						@Content(array = @ArraySchema(schema = 
							@Schema(implementation = Address.class))))
	})
	@GetMapping(value= "/addresses", produces = { "application/json"})
	public List<Address> getAllAddresses() {
		return (List<Address>) this.addressRepository.findAll();
	}
	
	@Operation(summary = "Find an address by id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successful operation", content =
					@Content(schema = @Schema(implementation = Address.class))),
			@ApiResponse(responseCode = "404", description = "Address not found")
	})
	@GetMapping(value = "/address/{id}", produces = { "application/json" })
	public ResponseEntity<Address> getAddressById(
			@Parameter(description="The address id must be provided to find a record (required).", required = true)
			@PathVariable(value = "id") Integer addressId
			)
		throws ResourceNotFoundException {
		
		Address address = this.addressRepository
							.findById(addressId)
							.orElseThrow(
									() -> new ResourceNotFoundException("Address not found on :: " + addressId)
									);
		
		return ResponseEntity.ok().body(address);
	}
	
	@Operation(summary = "Add a new address", tags = {"address"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Address created", content =
					@Content(schema = @Schema(implementation = Address.class))),
			@ApiResponse(responseCode = "400", description = "Invalid input"),
			@ApiResponse(responseCode = "405", description = "Validation exception")
	})
	@PostMapping(value = "/address", consumes = { "application/json" })
	public Address createAddress(
			@Parameter(description = "Address to add",
			required = true, schema = @Schema(implementation = Address.class,
			example = 
			"{ \"address\":\"string\", \"district\": \"string\", \"city\":\"string\", \"postalCode\": \"string\", \"personId\":0}"))
			@Valid @RequestBody Address address
			) {
		return this.addressRepository.save(address);
	}
	
	@Operation(summary = "Update an existing address", tags = {"address"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Address created", content =
					@Content(schema = @Schema(implementation = Address.class))),
			@ApiResponse(responseCode = "400", description = "Invalid input"),
			@ApiResponse(responseCode = "404", description = "Address not found"),
			@ApiResponse(responseCode = "405", description = "Validation exception"),
	})
	@PutMapping(value = "/address/{id}", consumes = { "application/json"})
	public ResponseEntity<Address> updateAddress(
			@Parameter(description="Address id required to be updated. Cannot be empty.", 
        		required=true)
			@PathVariable(value = "id") Integer addressId,
			@Parameter(description = "Address to add",
			required = true, schema = @Schema(implementation = Address.class,
			example = 
			"{ \"address\":\"string\", \"district\": \"string\", \"city\":\"string\", \"postalCode\": \"string\", \"personId\":0}"))
			@Valid @RequestBody Address addressDetails
			)
		throws ResourceNotFoundException {
		
		Address address = this.addressRepository
							.findById(addressId)
							.orElseThrow(
									() -> new ResourceNotFoundException("Address not found on :: " + addressId)
									);
		
		address.setAddress(addressDetails.getAddress());
		address.setDistrict(addressDetails.getDistrict());
		address.setCity(addressDetails.getCity());
		address.setPostalCode(addressDetails.getPostalCode());
		
		return ResponseEntity.ok().body(this.addressRepository.save(address));
	}
	
	@Operation(summary = "Deletes an address", tags = { "address" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "404", description = "Address not found") })
	@DeleteMapping("/address/{id}")
	public Map<String, Boolean> deleteAddress(
			@Parameter(description="Address id required to be deleted. Cannot be empty.",
        		required=true)
			@PathVariable(value = "id") Integer addressId
			)
			throws Exception {
		
		Address address = this.addressRepository
								.findById(addressId)
								.orElseThrow(
										() -> new ResourceNotFoundException("Address not found on:: " + addressId)
										);
		
		this.addressRepository.delete(address);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return response;
	}
}
