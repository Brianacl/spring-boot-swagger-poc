package com.tcs.poc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.poc.repository.PersonRepository;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

import com.tcs.poc.exception.ResourceNotFoundException;
import com.tcs.poc.model.Person;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Person", description = "Person API")
public class PersonController {
	@Autowired
	private PersonRepository personRepository;
	
	@Operation(summary = "Find all people registered with their addresses")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successful operation", content = 
						@Content(array = @ArraySchema(schema = 
							@Schema(implementation = Person.class))))
	})
	@GetMapping(value= "/people", produces = { "application/json"})
	public List<Person> getAllPerson() {
		return this.personRepository.findAll();
	}
	
	@Operation(summary = "Find a person by id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successful operation", content =
					@Content(schema = @Schema(implementation = Person.class))),
			@ApiResponse(responseCode = "404", description = "Person not found")
	})
	@GetMapping(value = "/person/{id}", produces = { "application/json"})
	public ResponseEntity<Person> getPersonById(
			@Parameter(description="The person id must be provided to find a record (required).", required = true)
			@PathVariable(value = "id") Integer personId
			)
		throws ResourceNotFoundException {
		Person person = this.personRepository
							.findById(personId)
							.orElseThrow(
									() -> new ResourceNotFoundException("User not found on :: " + personId)
									);
		
		return ResponseEntity.ok().body(person);
	}
	
	@Operation(summary = "Add a new person", tags = {"person"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Person created", content =
					@Content(schema = @Schema(implementation = Person.class))),
			@ApiResponse(responseCode = "400", description = "Invalid input"),
			@ApiResponse(responseCode = "405", description = "Validation exception")
	})
	@PostMapping(value = "/person", consumes = { "application/json"})
	public Person createPerson(
			@Parameter(description = "Person to add. (Addresses attribute could be null)",
					required = true, schema = @Schema(implementation = Person.class,
					example = 
					"{ \"firstName\":\"string\", \"lastName\": \"string\", \"age\": 0, \"bloodGroup\": \"O+\", \"addresses\":[]}"))
			@Valid @RequestBody Person person
			) {
		person.getAddresses().clear();
		return this.personRepository.save(person);
	}
	
	@Operation(summary = "Update an existing person", tags = {"person"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Person created", content =
					@Content(schema = @Schema(implementation = Person.class))),
			@ApiResponse(responseCode = "400", description = "Invalid input"),
			@ApiResponse(responseCode = "404", description = "Person not found"),
			@ApiResponse(responseCode = "405", description = "Validation exception"),
	})
	@PutMapping(value = "/person/{id}", consumes = { "application/json"})
	public ResponseEntity<Person> updatePerson(
			@Parameter(description="Person id required to be updated. Cannot be empty.", 
            	required=true)
			@PathVariable(value = "id") Integer personId,
			@Parameter(description="Person to update. Cannot null or empty. Don't need to send addresses", 
            	required=true, schema=@Schema(implementation = Person.class, 
            	example = "{ \"firstName\":\"string\", \"lastName\": \"string\", \"age\": 0, \"bloodGroup\": \"O+\"}"))
			@Valid @RequestBody Person personDetails
			)
		throws ResourceNotFoundException {
		
		Person person = this.personRepository
							.findById(personId)
							.orElseThrow(
									() -> new ResourceNotFoundException("User not found on :: " + personId)
									);
		
		person.setFirstName(personDetails.getFirstName());
		person.setLastName(personDetails.getLastName());
		person.setAge(personDetails.getAge());
		person.setBloodGroup(personDetails.getBloodGroup());
		
		return ResponseEntity.ok().body(this.personRepository.save(person));
	}
	
	@Operation(summary = "Deletes a person", tags = { "person" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "404", description = "Person not found") })
	@DeleteMapping("/person/{id}")
	public Map<String, Boolean> deletePerson(
			@Parameter(description="Person id required to be deleted. Cannot be empty.",
            	required=true)
			@PathVariable(value = "id") Integer personId
			)
			throws Exception {
		
		Person person = this.personRepository
								.findById(personId)
								.orElseThrow(
										() -> new ResourceNotFoundException("Person not found on:: " + personId)
										);
		
		this.personRepository.delete(person);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return response;
	}
}
