package com.tcs.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSwaggerPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSwaggerPocApplication.class, args);
	}

}
