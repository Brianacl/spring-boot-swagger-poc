package com.tcs.poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.poc.model.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> { }
